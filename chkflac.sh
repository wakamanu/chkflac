#!/usr/bin/env bash

# chkflac - Check a collection of FLAC files for errors 
#
# Copyright 2015 Daniel James <dj at slashspace dot org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
shopt -s globstar
shopt -s nullglob

PGM_NAME="chkflac"
VERSION="0.0.2"
LOG_FILE_NAME="chkflac.log"
FLAC_CMD="flac"
IGNORE_LOG=false
SKIP_ERRORS=false
SILENT=false
START_DIR="./"

function print_usage() {
    echo
    echo "$PGM_NAME - Check a collection of FLAC files for errors"
    echo
    echo "Usage:"
    echo " $PGM_NAME [options]... [directory]"
    echo
    echo "Options:"
    echo "  -h              Print this message"
    echo "  -m              Silent mode"
    echo "  -y              Ignore and overwrite any existing log files"
    echo "  -s              Skip errors"
    echo "  -v              Show version"
    echo "  -c [command]    Set the FLAC command (default: flac)"
    echo "  -l [name]       Set the base name of the log files (default: $LOG_FILE_NAME)"
    echo
    echo "Argument:"
    echo " The directory to start from (default: present working directory)"
    echo 
    echo "Examples:"
    echo " $PGM_NAME -m"
    echo "     Run $PGM_NAME silently" 
    echo " $PGM_NAME -s /path/to/my/music/ > $(date +"%Y-%m-%dT%H:%M:%S")_all.log"
    echo "     Skip all errors and redirect output to a file"
    echo
    echo "Note:"
    echo " By default, $PGM_NAME will skip direcories where a log file"
    echo " is already present."
    echo
}

function stdwrite() {
    if test $# -gt 0 && test -n "$1"
    then
        if [ "$SILENT" = false ] && test $# -eq 1
        then
            # Write to stdout
            printf "$1";
        else 
            # Write log entry
            if test $# -eq 2 && test -n "$2"
            then
                printf "$1" >> "$2"
            fi
        fi
    fi
}

while getopts ":hmysvc:l:" opt; do 
    case $opt in 
        h)
          print_usage
          exit 0
          ;;
        m)
          SILENT=true
          ;;
        y)
          IGNORE_LOG=true
          ;;
        s)
          SKIP_ERRORS=true
          ;;
        c)
          FLAC_CMD="$OPTARG"
          ;;
        l)
          LOG_FILE_NAME="$OPTARG"
          ;;
        v)
          echo "$VERSION"
          exit 0
          ;;
        \?)
          echo "Invalid option: -$OPTARG" >&2
          exit 1
          ;;
        :)
          echo "Option -$OPTARG requires an argument. See $PGM_NAME -h" >&2
          exit 1
          ;;
    esac
done

shift $((OPTIND-1));

if test $# -gt 1
then
    printf "Wrong number of arguments\n" >&2
    print_usage
    exit 1
else if test $# -eq 1  
    then 
        if test -d "$1"
        then
            START_DIR="$1"
        else
            printf "Directory not found: $1\n" >&2
            exit 1;
        fi
    fi
fi

if ! command -v "$FLAC_CMD" >/dev/null 2>&1 
then 
    echo >&2 "Command not found: $FLAC_CMD. Make sure it is installed and in your PATH.";
    exit 1
fi

chk_count=0
skp_count=0
err_count=0
flac_opts="-wst"
for dir in "$START_DIR"**/; do
    dir_count=0
    log_file="$dir$LOG_FILE_NAME"
    if [ "$IGNORE_LOG" = false ] && test -e "$log_file"
    then
        let "skp_count++"
        stdwrite "Log file found, skipping $dir\n"
    else
        for file in "$dir"*.flac; do
            if test $dir_count -eq 0 && test -e "$log_file"
            then 
                rm "$log_file"
            fi
            let "dir_count++"
            stdwrite "Checking file $file"
            err=$("$FLAC_CMD" "$flac_opts" "$file" 2>&1)
            if test $? -eq 0 
            then 
                stdwrite ": OK\n"
                stdwrite "$(basename "$file"): OK\n" "$log_file"
            else
                let "err_count++"
                err="$(echo -e "$err" | sed '/./,$!d')" # remove leading blank lines
                stdwrite ": ERROR\n$err\n"
                stdwrite "$err\n" "$log_file"
                if [ "$SKIP_ERRORS" = false ] 
                then 
                    exit 1
                fi
            fi
        done
        let "chk_count+=dir_count"
    fi
done
stdwrite "Files checked: $chk_count\nErrors: $err_count\nSkipped directories: $skp_count\n"
exit 0
