# chkflac - Check a collection of FLAC files for errors

##Usage

`chkflac [options]... [directory]`

##Options

* **-h**  
Show help  
* **-m**  
Silent mode  
* **-y**  
Ignore and overwrite existing log files  
* **-s**  
Skip errors  
* **-v**  
Show version
* **-c [command]**  
Set the FLAC command (default: flac)  
* **-l [name]**  
Set the base name of the log files (default: chkflac.log)  

##Argument

The directory to start from (default: present working directory)

##Examples

Run chkflac silently:


`chkflac -m`

Skip all errors and redirect output to a file:


`chkflac -s /path/to/my/music/ > 2015-03-07T18:08:46_all.log`

##Note

By default, chkflac will skip direcories where a log file
is already present.

##Author

Copyright 2015 Daniel James <dj at slashspace dot org>

##License

chkflac is released under the GNU General Public License, version 3 (see LICENSE).

